use std::time::SystemTime;
use tiny_blockchain::{blockchain::{Block}, utils};

#[test]
fn eq_blocks_should_have_eq_hash() {
  let time_stamp = SystemTime::now();
  let block1 = Block {
    index: 0,
    time_stamp: time_stamp,
    data: String::from(""),
    previous_hash: None
  };
  let block2 = Block {
    index: 0,
    time_stamp: time_stamp,
    data: String::from(""),
    previous_hash: None
  };

  let hash1 = utils::calculate_hash(&block1);
  let hash2 = utils::calculate_hash(&block2);

  assert_eq!(hash1, hash2);
}

#[test]
fn dif_blocks_should_have_dif_hash() {
  let time_stamp = SystemTime::now();
  let block1 = Block {
    index: 0,
    time_stamp: time_stamp,
    data: String::from("data1"),
    previous_hash: None
  };
  let block2 = Block {
    index: 0,
    time_stamp: time_stamp,
    data: String::from("data2"),
    previous_hash: None
  };

  let hash1 = utils::calculate_hash(&block1);
  let hash2 = utils::calculate_hash(&block2);

  assert_ne!(hash1, hash2);
}