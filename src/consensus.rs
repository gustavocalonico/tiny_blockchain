use crate::blockchain::{Blockchain};

fn find_new_chains() -> Vec<Blockchain> {
  let other_chains : Vec<Blockchain> = Vec::new();

  // TODO: implement other chains finding
  other_chains
}

fn longest_chain(found_chains : Vec<&Blockchain>) -> Blockchain {
  let mut longest : usize = 0;
  let mut size : usize = 0;
  for (pos, chain) in found_chains.iter().enumerate() {
    let len : usize = chain.len();
    if size < len {
      size = len;
      longest = pos;
    }
  }

  let longest_chain = found_chains[longest].clone();
  longest_chain
}

#[cfg(test)]
mod tests {
  use crate::consensus::longest_chain;
  use crate::blockchain::{Blockchain, Block, generate_genesis_block};
  use std::time::SystemTime;
  use crate::utils::calculate_hash;

  #[test]
  fn finds_longest_chain() {
    let genesis1 = generate_genesis_block();
    let genesis2 = generate_genesis_block();
    let index = genesis1.index;
    let block = Block {
      index: index + 1,
      time_stamp: SystemTime::now(),
      data: String::from("data"),
      previous_hash: Some(calculate_hash(&genesis2))
    };

    let blockchain1 = vec![
      genesis1,
      block
    ];

    let blockchain2 = vec![
      genesis2
    ];

    let longest = longest_chain(vec![&blockchain1, &blockchain2]);
    assert_eq!(longest.len(), blockchain1.len())
  }
}