use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

pub fn calculate_hash<T: Hash>(t: &T) -> u64 {
  let mut s = DefaultHasher::new();
  t.hash(&mut s);
  s.finish()
}

#[cfg(test)]
mod tests {
  use crate::utils::calculate_hash;

  #[test]
  fn eq_data_should_have_eq_hash() {
    let data = String::from("data");
    let hash1 = calculate_hash(&data);
    let hash2 = calculate_hash(&data);
    assert_eq!(hash1, hash2);
  }

  #[test]
  fn dif_data_should_hav_dif_hash() {
    let data1 = String::from("data1");
    let data2 = String::from("data2");
    let hash1 = calculate_hash(&data1);
    let hash2 = calculate_hash(&data2);
    assert_ne!(hash1, hash2);
  }
}