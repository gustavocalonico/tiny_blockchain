use crate::utils::calculate_hash;
use std::time::SystemTime;

#[derive(Hash)]
#[derive(Clone)]
pub struct Block {
  pub index: u32,
  pub time_stamp: SystemTime,
  pub data: String,
  pub previous_hash: Option<u64>,
}

pub type Blockchain = Vec<Block>;

pub fn generate_genesis_block() -> Block {
  let genesis = Block { 
    index: 0,
    time_stamp: SystemTime::now(),
    data: String::from("Genesis"),
    previous_hash: None,
  };
  genesis
}

pub fn generate_next_block(block : &Block, data : String) -> Block {
  let next = Block {
    index: block.index + 1,
    time_stamp: SystemTime::now(),
    data: data,
    previous_hash: Some(calculate_hash(block))
  };
  next
} 

#[cfg(test)]
mod tests {
  use crate::blockchain::{generate_genesis_block, generate_next_block};
  use crate::utils::calculate_hash;

  #[test]
  fn generates_genesis_block() {
    let genesis = generate_genesis_block();

    assert_eq!(genesis.index, 0);
    assert_eq!(genesis.data, "Genesis");
    assert_eq!(genesis.previous_hash, None);
  }

  #[test]
  fn generates_next_block() {
    let genesis = generate_genesis_block();
    let next = generate_next_block(&genesis, String::from("data"));
    let genesis_hash = calculate_hash(&genesis);

    assert_eq!(next.index, 1);
    assert_eq!(next.data, "data");
    assert_eq!(next.previous_hash.unwrap(), genesis_hash);
  }
}